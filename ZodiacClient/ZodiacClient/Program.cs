﻿using Grpc.Net.Client;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using ZodiacService;

namespace ZodiacClient
{
    class Program
    {
        public static bool Validation(string clientDate)
        {
            if (clientDate == "")
                return false;
            var pattern = "^(((0?[1-9]|1[012])/(0?[1-9]|1\\d|2[0-8])|(0?[13456789]|1[012])/(29|30)|(0?[13578]|1[02])/31)/(19|[2-9]\\d)\\d{2}|0?2/29/((19|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|(([2468][048]|[3579][26])00)))$";
            Regex regex = new Regex(pattern);
            return regex.IsMatch(clientDate);

        }

        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new Zodiac.ZodiacClient(channel);

            string clientDate;
            clientDate = Console.ReadLine();

            while (!Validation(clientDate))
            {
                Console.WriteLine("Enter a valid date ");
                clientDate = Console.ReadLine();
            }

            var response = await client.ZodiacSignAsync(new DateRequest { Date = clientDate });
            Console.WriteLine(response);
        }
    }
}
