using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacService
{
    public class ZodiacServer : Zodiac.ZodiacBase
    {
        private readonly ILogger<ZodiacServer> _logger;
        public ZodiacServer(ILogger<ZodiacServer> logger)
        {
            _logger = logger;
        }

        public List<Tuple<string, string, string>> ReadFromFile()
        {
            List<Tuple<string, string, string>> ourListForTheZodiacSigns = new List<Tuple<string, string, string>>();
            char[] separator = new char[] { ' ' };
            string[] pieceByPiece;
            string substringForLine;
            string pathForTheFile = Directory.GetCurrentDirectory() + @"\Resources\IntervalsForZodiacSigns.txt";
            try
            {
                using (var reader = new StreamReader(pathForTheFile))
                {
                    while (!reader.EndOfStream)
                    {
                        substringForLine = reader.ReadLine();
                        pieceByPiece = substringForLine.Split(separator);
                        ourListForTheZodiacSigns.Add(new Tuple<string, string, string>(pieceByPiece[0], pieceByPiece[1], pieceByPiece[2]));
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            return ourListForTheZodiacSigns;
        }

        public string SignFromZodiac(List<Tuple<string, string, string>> ListForTheZodiacSigns, string actualDateToVerify)
        {
            char[] separator = new char[] { '/' };
            string[] pieceByPiece;
            string[] pieceByPieceForTheSubstring1;
            string[] pieceByPieceForTheSubstring2;
            string resultForZodiacSign = "Undefined";

            pieceByPiece = actualDateToVerify.Split(separator);

            foreach (var substring in ListForTheZodiacSigns)
            {
                pieceByPieceForTheSubstring1 = substring.Item1.Split(separator);
                pieceByPieceForTheSubstring2 = substring.Item2.Split(separator);
                if (Convert.ToInt32(pieceByPiece[0]) >= Convert.ToInt32(pieceByPieceForTheSubstring1[0]) &&
                    Convert.ToInt32(pieceByPiece[0]) <= Convert.ToInt32(pieceByPieceForTheSubstring2[0]) &&
                    (Convert.ToInt32(pieceByPiece[1]) <= Convert.ToInt32(pieceByPieceForTheSubstring2[1]) ||
                    Convert.ToInt32(pieceByPiece[1]) >= Convert.ToInt32(pieceByPieceForTheSubstring1[1]))
                    )
                {
                    resultForZodiacSign = substring.Item3;
                }
            }
            return resultForZodiacSign;
        }

        public override Task<ZodiacReply> ZodiacSign(DateRequest request, ServerCallContext context)
        {
            return Task.FromResult(new ZodiacReply
            {
                ZodiacSign = SignFromZodiac(ReadFromFile(), request.Date)
            });
        }
    }
}
